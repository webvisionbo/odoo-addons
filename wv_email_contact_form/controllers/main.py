# -*- coding: utf-8 -*-

from openerp import SUPERUSER_ID
from openerp.addons.web import http
from openerp.addons.web.http import request
from openerp.addons.website_form.controllers.main import WebsiteForm

class WebsiteFormEmail(WebsiteForm):
    # Check and insert values from the form on the model <model>
    @http.route('/website_form/<string:model_name>', type='http', auth="public", methods=['POST'], website=True)
    def website_form(self, model_name, **kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        if model_name == 'crm.lead':
            email_sender = pool['ir.config_parameter'].get_param(cr, uid, "mail.contact.form.sender")

            email_dict = kwargs
            email_content = """
            <b>Contact Form</b><br/>
            ------------
            <br/>
            """

            for k, v in email_dict.iteritems():
                email_content += """<br/>
                <b>%s:</b> %s
                """ % (k, v)

            partner_recipients = pool['contact.form.recipients'].search(cr, 1, [])
            recipients = []
            for p in pool['contact.form.recipients'].browse(cr, 1, partner_recipients):
                recipients.append((4, p.partner_id.id))

            mail_values = {
                'subject': "Contact Form",
                'body': email_content or '',
                'body_html': email_content or '',
                #'author_id': self.author_id.id,
                'email_from': email_sender,
                #'record_name': self.record_name,
                'auto_delete': False,
                'message_type': 'email',
                #'no_auto_thread': self.no_auto_thread,
                #'mail_server_id': self.mail_server_id.id,
                'recipient_ids': recipients,
            }

            mail_id = pool['mail.mail'].create(cr, 1, mail_values)
            pool['mail.mail'].send(cr, 1, mail_id)

            return super(WebsiteFormEmail, self).website_form(model_name, **kwargs)
        else:
            return super(WebsiteFormEmail, self).website_form(model_name, **kwargs)