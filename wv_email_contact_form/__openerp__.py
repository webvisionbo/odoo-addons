# -*- coding: utf-8 -*-
##############################################################################
#
#    Poiesis Consulting, OpenERP Partner
#    Copyright (C) 2013 Poiesis Consulting (<http://www.poiesisconsulting.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'WebVision Website Email Contact Form',
    'version': '1.0',
    'category': 'website',
    'sequence': 1,
    'summary': 'Contact Form sends email to certain emails',
    'description': """
WebVision Website Email Contact Form
====================================

    """,
    'author': 'Grover Menacho',
    'website': 'http://www.grovermenacho.com',
    'depends': ['web','website_form','mail'],
    'data': ['views/contact_form_recipients_view.xml',
             'security/ir.model.access.csv',],
    'installable': True,
    'active': False,
    'application': True,
#    'certificate': 'certificate',
}