# -*- coding: utf-8 -*-
##############################################################################
#
#    Poiesis Consulting, OpenERP Partner
#    Copyright (C) 2013 Poiesis Consulting (<http://www.poiesisconsulting.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'WebVision Website Lock Form',
    'version': '1.0',
    'category': 'website',
    'sequence': 1,
    'summary': 'It blocks page when user sends a form',
    'description': """
WebVision Website Form
======================
Actually when someone sends Contact Us, page will stay editable and it seems that there is no answer if server delays.
This module will block the page.
    """,
    'author': 'Grover Menacho',
    'website': 'http://www.grovermenacho.com',
    'depends': ['web','website_form'],
    'data': ['views/wv_website_form.xml'],
    'installable': True,
    'active': False,
    'application': True,
    'images': ['images/banner.png'],

#    'certificate': 'certificate',
}